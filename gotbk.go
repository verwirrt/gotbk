package gotbk

import (
	"os"

	"github.com/subosito/gotenv"

	pddsdk "gitee.com/verwirrt/gotbk/utils/pdd"
	topsdk "gitee.com/verwirrt/gotbk/utils/taobao"
)

func init() {
	gotenv.Load()
}

func (*Juhe) Pdd(method string, data map[string]interface{}) (string, error) {
	tb_appkey := os.Getenv("pdd_ClientId")
	tb_appSecret := os.Getenv("pdd_ClientSecret")
	url := os.Getenv("pdd_url")

	client := pddsdk.NewDefaultTopClient(tb_appkey, tb_appSecret, url, 2000, 2000)

	// // canshu
	// data := map[string]interface{}{}
	// method := "pdd.ddk.goods.pid.query"
	res, err := client.Execute(method, data, map[string]interface{}{})
	return res, err
}

// demo
func (*Juhe) Taobao(method string, data map[string]interface{}) (string, error) {
	tb_appkey := os.Getenv("tb_appkey")
	tb_appSecret := os.Getenv("tb_appSecret")
	url := os.Getenv("tb_url")
	client := topsdk.NewDefaultTopClient(tb_appkey, tb_appSecret, url, 2000, 2000)
	// canshu
	// data := map[string]interface{}{
	// 	"adzone_id":   "11165650114",
	// 	"material_id": "1",
	// }
	// method := "taobao.tbk.dg.optimus.material"
	res, err := client.Execute(method, data, map[string]interface{}{})
	return res, err
}

type Juhe struct {
}

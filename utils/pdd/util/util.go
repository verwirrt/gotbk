package util

import (
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"sort"
	"strings"
)

func Md5(s string) string {
	h := md5.New()
	h.Write([]byte(s))
	return hex.EncodeToString(h.Sum(nil))
}

func GetSign(publicParam map[string]interface{}, data map[string]interface{}, secret string) string {
	var allParamMap = make(map[string]interface{})
	for k, v := range data {
		allParamMap[k] = v
	}
	for k, v := range publicParam {
		allParamMap[k] = v
	}
	var keyList []string
	for k := range allParamMap {
		keyList = append(keyList, k)
	}
	sort.Strings(keyList)
	var signStr = ""
	for _, key := range keyList {
		value := allParamMap[key]
		signStr = signStr + fmt.Sprintf("%v%v", key, value)
		//if(value != ""){
		//	signStr = signStr + fmt.Sprintf("%v%v", key, value)
		//}
	}
	// fmt.Println(signStr)
	sign := strings.ToUpper(Md5(secret + signStr + secret))
	return sign
}
